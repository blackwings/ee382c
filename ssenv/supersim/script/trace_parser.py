from sys import argv
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np

num_nodes = 64
concentration = 3
num_terms = num_nodes #num_nodes * concentration
traffic = np.zeros((num_terms,num_terms))

def parse():
    msg = open ('{0}/{1}_messages.mpf'.format(folder,app))
    for line in msg:
        if ('+M' in line):
            arr = line.split(",")
            src = int(arr[2]) / 3
            dst = int(arr[3]) / 3
            traffic[src][dst] += 1 
    msg.close()
    print "------finish parsing------"


def genCVS(filename):
    csv = open(filename, 'w')
    line = "Terminals"
    for i in range(0, num_terms):
        line += ", {0}".format(i)
    csv.write(line)
    for i in range(0, num_terms):
        line = "{0}".format(i)
        for j in range(0, num_terms):
            line += ",{0}".format(traffic[i][j])
        csv.write(line)
    csv.close()

#nba_sort
def plotMatrix():
    # Plot it out
    fig, ax = plt.subplots()
    heatmap = ax.pcolor(traffic, cmap=plt.cm.Blues, alpha=1)
    
    # Format
    fig = plt.gcf()
    fig.set_size_inches(11, 11)
    
    # turn off the frame
    ax.set_frame_on(False)
    
    # put the major ticks at the middle of each cell
    ax.set_yticks(np.arange(0,len(traffic)+1,len(traffic)/8), minor=True)
    ax.set_xticks(np.arange(0,len(traffic)+1,len(traffic)/8), minor=True)

    # want a more natural, table-like display
    #ax.invert_yaxis()
    #ax.xaxis.tick_top()
    
    ax.grid(True)

    ax.set_title('Traffic Matrix ({0})'.format(app))
    ax.set_xlabel('Source')
    ax.set_ylabel('Destination')
    ax.set_xlim([0,len(traffic)])
    ax.set_ylim([0,len(traffic)])
    
    plt.savefig('{0}/{1}_mat.png'.format(folder,app))
    print "------finish plotting------"
    
def main(argv):
    global folder,app
    script,folder,app= argv
    if (len(argv)!=3):
        print "usage:<{0}> <{1}>".format('folder','app')
    parse()

    #genCVS(csv)
    plotMatrix()

main (argv)
